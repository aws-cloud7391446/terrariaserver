module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"

  name = var.name
  cidr = local.vpc_cidr

  azs                      = [var.availability_zone]
  public_subnets           = [local.public_subnet_cidr]
  private_subnets          = [local.private_subnet_cidr]

  create_igw               = true
  enable_nat_gateway       = false
  map_public_ip_on_launch  = true

  default_security_group_ingress = [{
    protocol    = "tcp"
    cidr_blocks = "0.0.0.0/0"
    from_port   = 7777
    to_port     = 7777
  }, {
    protocol    = "tcp"
    cidr_blocks = "0.0.0.0/0"
    from_port   = 22
    to_port     = 22
  }, {
    protocol    = "icmp"
    cidr_blocks = "0.0.0.0/0"
    from_port   = -1
    to_port     = -1
  }, {
    protocol    = "tcp"
    cidr_blocks = local.vpc_cidr
    from_port   = 80
    to_port     = 80
  }, {
    protocol    = "tcp"
    cidr_blocks = local.vpc_cidr
    from_port   = 443
    to_port     = 443
  }]

  default_security_group_egress = [{
    protocol    = -1
    cidr_blocks = "0.0.0.0/0"
    from_port   = 0
    to_port     = 0
  }]

  tags = {
    TerrariaServer = var.name
  }
}

resource "aws_route" "ec2_proxy" {
  route_table_id         = module.vpc.private_route_table_ids[0]
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = aws_instance.this.primary_network_interface_id
}