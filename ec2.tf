data "aws_ami" "this" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "tls_private_key" "this" {
  algorithm = "ED25519"
}

resource "local_file" "ssh_private_key" {
  content  = sensitive(tls_private_key.this.private_key_openssh)
  filename = "${path.root}/ssh.key"
}

resource "aws_secretsmanager_secret" "ssh_private_key" {
  name                    = "/public/${var.name}/ssh_private_key"
  recovery_window_in_days = 0

  policy = null

  tags = {
    TerrariaServer = var.name
  }
}

resource "aws_secretsmanager_secret_version" "ssh_private_key" {
  secret_id     = aws_secretsmanager_secret.ssh_private_key.id
  secret_string = sensitive(base64encode(tls_private_key.this.private_key_openssh))
}

resource "aws_key_pair" "this" {
  key_name   = "${var.name}-SSH-key"
  public_key = tls_private_key.this.public_key_openssh
}


data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "ec2_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:ListBucket"
    ]

    resources = ["${aws_s3_bucket.this.arn}", "${aws_s3_bucket.this.arn}/*"]
  }
}

resource "aws_iam_role" "ec2" {
  name = "${var.name}EC2"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json

  tags = {
    TerrariaServer = var.name
  }
}

resource "aws_iam_role_policy" "ec2" {
  name   = "${var.name}EC2"
  role   = aws_iam_role.ec2.id
  policy = data.aws_iam_policy_document.ec2_policy.json
}

resource "aws_iam_instance_profile" "ec2" {
  name = "${var.name}EC2"
  role = aws_iam_role.ec2.name

  tags = {
    TerrariaServer = var.name
  }

  depends_on = [
    aws_iam_role_policy.ec2
  ]
}


resource "aws_instance" "this" {
  ami           = data.aws_ami.this.id
  instance_type = var.EC2_instance_type
  key_name      = aws_key_pair.this.key_name

  iam_instance_profile = aws_iam_instance_profile.ec2.name

  vpc_security_group_ids      = [module.vpc.default_security_group_id]
  subnet_id                   = module.vpc.public_subnets[0]
  private_ip                  = local.private_ip
  source_dest_check           = false

  user_data = file("${path.root}/user-data.sh")

  root_block_device {
    volume_size = 8

    tags = {
      Name           = var.name
      TerrariaServer = var.name
    }
  }

  metadata_options {
    http_tokens = "required"
  }

  tags = {
    Name           = var.name
    TerrariaServer = var.name
  }
}

resource "aws_ec2_instance_state" "this" {
  count = var.server_enabled ? 1 : 0

  instance_id = aws_instance.this.id
  state       = local.server_state
}

data "aws_instance" "this" {
  instance_id = var.server_enabled ? aws_ec2_instance_state.this[0].id : aws_instance.this.id
}