variable "name" {
  default     = "TerrariaServer"
  description = "Name used for created resources"
  type        = string
}

variable "region" {
  default     = "eu-west-2"
  description = "Region used"
  type        = string
}

variable "availability_zone" {
  default     = "eu-west-2a"
  description = "Availability zone used"
  type        = string
}


# EC2
variable "EC2_instance_type" {
  default     = "t2.medium"
  description = "EC2 Instance type"
  type        = string
}


# Server
variable "server_enabled" {
  default     = true
  description = "Terraria Server state"
  type        = bool
}

variable "server_version" {
  default     = "v1.4.4.9"
  description = "Terraria Server Version"
  type        = string
}

variable "server_tmod" {
  default     = false
  description = "Build tModLoader Server instead. Automatic update via terraform apply"
  type        = bool
}