data "aws_subnet" "private_subnet" {
  id = module.vpc.private_subnets[0]
}

data "aws_iam_policy_document" "codebuild_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "codebuild_policy" {
  statement {
    effect = "Allow"

    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeDhcpOptions",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeVpcs",
    ]

    resources = ["*"]
  }

  statement {
    effect    = "Allow"
    actions   = ["ec2:CreateNetworkInterfacePermission"]
    resources = ["arn:aws:ec2:${var.region}:${local.account_id}:network-interface/*"]

    condition {
      test     = "StringEquals"
      variable = "ec2:Subnet"
      values   = [data.aws_subnet.private_subnet.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "ec2:AuthorizedService"
      values   = ["codebuild.amazonaws.com"]
    }
  }

  statement {
    effect    = "Allow"
    actions   = ["ssm:GetParameters"]
    resources = ["arn:aws:ssm:${var.region}:${local.account_id}:parameter/CodeBuild/*"]
  }

  statement {
    effect    = "Allow"
    actions   = ["s3:*"]
    resources = ["${aws_s3_bucket.this.arn}", "${aws_s3_bucket.this.arn}/*"]
  }

  statement {
    effect    = "Allow"
    actions   = ["secretsmanager:GetSecretValue"]
    resources = [aws_secretsmanager_secret.ssh_private_key.arn]
  }
}

resource "aws_iam_role" "codebuild" {
  name               = "${var.name}CodeBuild"
  assume_role_policy = data.aws_iam_policy_document.codebuild_assume_role.json
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "${var.name}CodeBuild"
  role   = aws_iam_role.codebuild.id
  policy = data.aws_iam_policy_document.codebuild_policy.json
}

resource "aws_codebuild_project" "this" {
  name           = var.name
  build_timeout  = 5
  service_role   = aws_iam_role.codebuild.arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    type                        = "LINUX_CONTAINER"
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:4.0"
    privileged_mode             = false
  }

  source {
    type = "CODEPIPELINE"
  }

  vpc_config {
    vpc_id  = module.vpc.vpc_id
    subnets = module.vpc.private_subnets

    security_group_ids = [module.vpc.default_security_group_id]
  }

  logs_config {
    cloudwatch_logs {
      status = "DISABLED"
    }

    s3_logs {
      status = "ENABLED"
      location = "${aws_s3_bucket.this.bucket}/logs"
    }
  }

  tags = {
    TerrariaServer = var.name
  }

  depends_on = [
    aws_iam_role_policy.codebuild
  ]
}


data "aws_iam_policy_document" "codedeploy_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "codedeploy_policy" {
  statement {
    effect = "Allow"

    actions = [
      "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus",
      "ec2:TerminateInstances",
      "tag:GetResources",
      "sns:Publish",
      "cloudwatch:DescribeAlarms",
      "cloudwatch:PutMetricAlarm",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role" "codedeploy" {
  name               = "${var.name}CodeDeploy"
  assume_role_policy = data.aws_iam_policy_document.codedeploy_assume_role.json
}

resource "aws_iam_role_policy" "codedeploy" {
  name   = "${var.name}CodeDeploy"
  role   = aws_iam_role.codedeploy.id
  policy = data.aws_iam_policy_document.codedeploy_policy.json
}

resource "aws_codedeploy_app" "this" {
  name             = var.name
  compute_platform = "Server"

  tags = {
    TerrariaServer = var.name
  }
}

resource "aws_codedeploy_deployment_group" "this" {
  app_name              = aws_codedeploy_app.this.name
  deployment_group_name = var.name
  service_role_arn      = aws_iam_role.codedeploy.arn

  outdated_instances_strategy = "UPDATE"

  ec2_tag_filter {
    key   = "TerrariaServer"
    value = var.name
    type  = "KEY_AND_VALUE"
  }

  tags = {
    TerrariaServer = var.name
  }

  depends_on = [
    aws_iam_role_policy.codedeploy
  ]
}


data "aws_iam_policy_document" "codepipeline_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "codepipeline_policy" {
  statement {
    effect    = "Allow"
    actions   = ["s3:*"]
    resources = ["${aws_s3_bucket.this.arn}", "${aws_s3_bucket.this.arn}/*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "codebuild:BatchGetBuilds",
      "codebuild:BatchGetBuildBatches",
      "codebuild:StartBuild",
      "codebuild:StartBuildBatch",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "codedeploy:CreateDeployment",
      "codedeploy:GetApplication",
      "codedeploy:GetApplicationRevision",
      "codedeploy:GetDeployment",
      "codedeploy:GetDeploymentConfig",
      "codedeploy:RegisterApplicationRevision",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role" "codepipeline" {
  name               = "${var.name}CodePipeline"
  assume_role_policy = data.aws_iam_policy_document.codepipeline_assume_role.json
}

resource "aws_iam_role_policy" "codepipeline" {
  name   = "${var.name}CodePipeline"
  role   = aws_iam_role.codepipeline.id
  policy = data.aws_iam_policy_document.codepipeline_policy.json
}

resource "aws_codepipeline" "this" {
  name     = var.name
  role_arn = aws_iam_role.codepipeline.arn

  artifact_store {
    location = aws_s3_bucket.this.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Revision"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        S3Bucket             = aws_s3_bucket.this.bucket
        S3ObjectKey          = "revision.zip"
        PollForSourceChanges = true
      }
    }

    action {
      name             = "Assets"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["assets_output"]

      configuration = {
        S3Bucket             = aws_s3_bucket.this.bucket
        S3ObjectKey          = "assets.zip"
        PollForSourceChanges = false
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output", "assets_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.this.name
        EnvironmentVariables = jsonencode([{
          name  = "SERVER_SSH_KEY"
          value = aws_secretsmanager_secret.ssh_private_key.name
          type  = "SECRETS_MANAGER"
        }])
        PrimarySource = "source_output"
      }
    }
  }

  dynamic "stage" {
    for_each = var.server_enabled ? [0] : []

    content {
      name = "Deploy"

      action {
        name            = "Deploy"
        category        = "Deploy"
        owner           = "AWS"
        provider        = "CodeDeploy"
        input_artifacts = ["build_output"]
        version         = "1"

        configuration = {
          ApplicationName     = var.name
          DeploymentGroupName = var.name
        }
      }
    }
  }

  tags = {
    TerrariaServer = var.name
  }

  depends_on = [
    aws_iam_role_policy.codepipeline,
    aws_codebuild_project.this,
    aws_codedeploy_deployment_group.this
  ]
}