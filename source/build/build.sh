#!/bin/bash
set -ex
source build/tfvars.sh
export SERVER_SSH_KEY


# RPC
function check_codedeploy_agent() {
    set -x
    if sudo service codedeploy-agent status ; then
        return
    fi

    sudo apt update && sudo apt install ruby-full -y
    wget ${AGENT_URL} && chmod +x ./install
    sudo ./install auto

    rm -rf install
    sudo service codedeploy-agent start || true
}

function disable_server() {
    while [[ -n $(screen -ls | grep -o '.terraria\s') ]] ; do
        screen -XS terraria stuff '\nexit\n'
        sleep 10
    done
    screen -dm sudo bash -c 'sleep 3; shutdown -P now'
}


# Build
function build_server_artifact() {
    local server_version="${1}"

    wget https://terraria.org/api/download/pc-dedicated-server/terraria-server-${server_version}.zip
    unzip "terraria-server-${server_version}.zip" "${server_version}/Linux/*"

    mv "${server_version}/Linux" terraria-server
    mv serverconfig.txt terraria-server/

    chmod +x terraria-server/TerrariaServer*
}

function build_server_artifact_tmod() {
    local server_version="${1}"

    wget https://github.com/tModLoader/tModLoader/releases/download/${server_version}/tModLoader.zip
    unzip -d terraria-server tModLoader.zip

    mv tmod-serverconfig.txt terraria-server/serverconfig.txt
    mv terraria-server/start-tModLoaderServer.sh terraria-server/TerrariaServer
    
    mv ${CODEBUILD_SRC_DIR_assets_output}/Mods ./Mods

    chmod +x terraria-server/TerrariaServer
}


echo "${SERVER_SSH_KEY}" | base64 -d > ssh.key && chmod 400 ssh.key
if   [[ ${SERVER_ENABLED} == true ]] ; then
    ssh -i ssh.key ${SERVER_USER}@${SERVER_ADDRESS} -T -oStrictHostKeyChecking=no \
        "$(typeset -f check_codedeploy_agent | envsubst | tr -d '\r'); check_codedeploy_agent"
    if [[ -z ${SERVER_TMOD_VERSION} ]] ; then
        build_server_artifact "${SERVER_VERSION}"
    else
        build_server_artifact_tmod "${SERVER_TMOD_VERSION}"
    fi

elif [[ ${SERVER_ENABLED} == false ]] ; then
    ssh -i ssh.key ${SERVER_USER}@${SERVER_ADDRESS} -T -oStrictHostKeyChecking=no \
        "$(typeset -f disable_server | envsubst | tr -d '\r'); disable_server"
fi