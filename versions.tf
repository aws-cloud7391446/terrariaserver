terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.31.0"
    }

    curl = {
      source = "anschoewe/curl"
      version = "1.0.2"
    }
  }
}