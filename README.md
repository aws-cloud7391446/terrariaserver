# Terraria Server
![Terraria Server](/uploads/50c47e7d3c58b0fc1d26f73bee0e3317/terraria.jpg)

Terraform Application for fast deployment of a Terraria Server with a tModLoader support on the AWS cloud  

Control your server state by setting up Terraform variables and using `terraform apply` command  

Keep your server enabled by using `server_enabled` variable while playing it with your friends
and disable it whenever you want  

Make sure to run `terraform apply` a few times when disabling your server, output variable `server_state` value should be
`stopped` after some time, which means EC2 instance is stopped


## Vanilla Terraria Server
Set `server_tmod` variable to `false` and specify a Terraria version via `server_version` variable


## tModLoader Server
Set `server_tmod` variable to `true` and put your mods into `source/assets/Mods` folder to upload them on a server