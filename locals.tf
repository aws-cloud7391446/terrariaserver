locals {
  # VPC
  vpc_cidr            = "10.77.0.0/16"
  public_subnet_cidr  = "10.77.0.0/24"
  private_subnet_cidr = "10.77.1.0/24"

  # EC2
  private_ip = "10.77.0.20"

  # Server
  server_state   = var.server_enabled ? "running" : "stopped"
  server_version = replace(var.server_version, "/[.v]/", "")
  server_user    = "ubuntu"

  server_tmod_version = var.server_tmod ? jsondecode(data.curl.tmod_latest[0].response).tag_name : ""
  server_tmod_mods    = var.server_tmod && length(fileset("${path.root}/source/assets", "**")) != 0

  # Other
  account_id = data.aws_caller_identity.this.account_id
  codedeploy_agent_map = {
    us-east-1 = "aws-codedeploy-us-east-1"
    us-east-2 = "aws-codedeploy-us-east-2"
    us-west-1 = "aws-codedeploy-us-west-1"
    us-west-2 = "aws-codedeploy-us-west-2"
    af-south-1 = "aws-codedeploy-af-south-1"
    ap-east-1 = "aws-codedeploy-ap-east-1"
    ap-south-2 = "aws-codedeploy-ap-south-2"
    ap-southeast-3 = "aws-codedeploy-ap-southeast-3"
    ap-southeast-4 = "aws-codedeploy-ap-southeast-4"
    ap-south-1 = "aws-codedeploy-ap-south-1"
    ap-northeast-3 = "aws-codedeploy-ap-northeast-3"
    ap-northeast-2 = "aws-codedeploy-ap-northeast-2"
    ap-southeast-1 = "aws-codedeploy-ap-southeast-1"
    ap-southeast-2 = "aws-codedeploy-ap-southeast-2"
    ap-northeast-1 = "aws-codedeploy-ap-northeast-1"
    ca-central-1 = "aws-codedeploy-ca-central-1"
    eu-central-1 = "aws-codedeploy-eu-central-1"
    eu-west-1 = "aws-codedeploy-eu-west-1"
    eu-west-2 = "aws-codedeploy-eu-west-2"
    eu-south-1 = "aws-codedeploy-eu-south-1"
    eu-west-3 = "aws-codedeploy-eu-west-3"
    eu-south-2 = "aws-codedeploy-eu-south-2"
    eu-north-1 = "aws-codedeploy-eu-north-1"
    eu-central-2 = "aws-codedeploy-eu-central-2"
    il-central-1 = "aws-codedeploy-il-central-1"
    me-south-1 = "aws-codedeploy-me-south-1"
    me-south-1 = "aws-codedeploy-me-central-1"
    sa-east-1 = "aws-codedeploy-sa-east-1"
    us-gov-east-1 = "aws-codedeploy-us-gov-east-1"
    us-gov-west-1 = "aws-codedeploy-us-gov-west-1"
  }
  codedeploy_agent_url = "https://${local.codedeploy_agent_map[var.region]}.s3.${var.region}.amazonaws.com/latest/install"
}


data "aws_caller_identity" "this" {}

data "curl" "tmod_latest" {
  count = var.server_tmod ? 1 : 0

  http_method = "GET"
  uri = "https://api.github.com/repos/tModLoader/tModLoader/releases/latest"
}