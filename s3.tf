resource "aws_s3_bucket" "this" {
  bucket_prefix = "${lower(var.name)}-"
  force_destroy = true

  tags = {
    Name           = var.name
    TerrariaServer = var.name
  }
}

resource "aws_s3_bucket_versioning" "this" {
  bucket = aws_s3_bucket.this.id

  versioning_configuration {
    status = "Enabled"
  }
}


data "archive_file" "assets" {
  type        = "zip"
  output_path = "${path.root}/assets.zip"
  source_dir  = "${path.root}/source/assets"

  excludes = var.server_tmod ? null : ["Mods"]
}

module "revision_template" {
  source   = "hashicorp/dir/template"
  base_dir = "${path.root}/source"

  template_vars = {
    server_user           = local.server_user
    agent_url             = local.codedeploy_agent_url
    server_enabled        = var.server_enabled
    server_version        = local.server_version
    server_address        = aws_instance.this.private_ip
    server_assets_md5     = data.archive_file.assets.output_md5
    server_tmod_version   = local.server_tmod_version
    server_tmod_mods      = local.server_tmod_mods
  }
}

data "archive_file" "revision" {
  type        = "zip"
  output_path = "${path.root}/revision.zip"

  dynamic "source" {
    for_each = {for k, v in module.revision_template.files : k => v if ! startswith(k, "assets/")}

    content {
      filename = source.key
      content  = source.value.content != null ? source.value.content : file("${path.root}/source/${source.key}")
    }
  }
}

resource "aws_s3_object" "assets" {
  key    = "assets.zip"
  bucket = aws_s3_bucket.this.id
  source = data.archive_file.assets.output_path

  etag = data.archive_file.assets.output_md5

  tags = {
    Name           = var.name
    TerrariaServer = var.name
  }

  depends_on = [
    aws_s3_bucket_versioning.this,
    data.archive_file.assets,
    aws_codepipeline.this
  ]
}

resource "aws_s3_object" "revision" {
  key    = "revision.zip"
  bucket = aws_s3_bucket.this.id
  source = data.archive_file.revision.output_path

  etag = data.archive_file.revision.output_md5

  tags = {
    Name           = var.name
    TerrariaServer = var.name
  }

  depends_on = [
    aws_s3_bucket_versioning.this,
    aws_s3_object.assets,
    data.archive_file.revision,
    aws_codepipeline.this
  ]
}