output "public_ip" {
  value = data.aws_instance.this.public_ip
}

output "server_state" {
  value = data.aws_instance.this.instance_state
}